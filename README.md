# SLogOSBackend

OSLog backend for SLog. Suitable for Swift-based apps for Apple platforms: iOS, macOS, tvOS, watchOS.

## Getting started
Because the backend uses private Apple's API in implementation, we would recommend avoid usage of it production environment. It could be achieved by conditional compilation:
``` swift
let someOtherBackend = ...
#if DEBUG
    let osLogBackend = OSLogBackend(.debug)
    LoggingSystem.bootstrap { MultiplexLogBackend[osLogBackend, someOtherBackend]) }
#else
// add only appropriate for production backend
    LoggingSystem.bootstrap { MultiplexLogBackend[someOtherBackend]) }
#endif
```