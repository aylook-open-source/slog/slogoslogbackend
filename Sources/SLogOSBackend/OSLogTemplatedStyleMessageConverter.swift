import Foundation
import SLog

public class OSLogTemplatedStyleMessageConverter: TemplatedMessageConverter {
    public static let shared: TemplatedMessageConverter = OSLogTemplatedStyleMessageConverter()

    private static let placeholdersRegEx = "%(\\{private\\}|\\{public\\})?(h|hh|l|ll|q|L|z|t|j)?[@dDuUxXoOfFeEgGcCsSpaA]"
    private static let privatePlaceholderDeterminer = "%{private}"
    static let privatePlaceholderReplacement = "#private#"

    private init() { }

    public func convertToString(templated: String, arguments: [TypeWrapperRepresentable]) -> String {
        var tempTemplatedString = templated
            .replacingOccurrences(of: "{public}", with: "")

        var args = arguments.map { $0.typeWrappedValue.cVarArg }

        // all these operation are quite expensive, because they use RegEx
        // TODO: refactor if better solution appears.
        if tempTemplatedString.contains(Self.privatePlaceholderDeterminer) {
            let privatePlaceholdersIndices = findPrivatePlaceholdersWithIndices(tempTemplatedString)
            tempTemplatedString = replacePrivatePlaceholders(of: tempTemplatedString, privatePlaceholdersIndices)

            args = filterPrivateArgs(privatePlaceholdersIndices.map { $0.offset }, args)
        }

        return String(format: tempTemplatedString, arguments: args)
    }

    private func replacePrivatePlaceholders(of templated: String,
                                            _ placeholdersWithIndex: [(offset: Int, value: String)]) -> String {
        var result = templated
        for (_, placeholder) in placeholdersWithIndex {
            // Not really efficient way of replacing.
            // TODO: refactor if better solution appears.
            result = result.replacingOccurrences(of: placeholder, with: Self.privatePlaceholderReplacement)
        }
        return result
    }

    private func findPrivatePlaceholdersWithIndices(_ templated: String) -> [(offset: Int, value: String)] {
        templated.matches(for: Self.placeholdersRegEx)
            .enumerated()
            .filter { $0.element.contains(Self.privatePlaceholderDeterminer)}
            .map { ($0.offset, $0.element) }
    }

    private func filterPrivateArgs(_ privatePlaceholdersIndices: [Int], _ arguments: [CVarArg]) -> [CVarArg] {
        // remove arguments that are private
        return arguments
            .enumerated()
            .filter { !privatePlaceholdersIndices.contains($0.offset) }
            .map { $0.1 }
    }
}

fileprivate extension String {
    func matches(for regex: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: self,
                                        range: NSRange(self.startIndex..., in: self))
            return results.map {
                String(self[Range($0.range, in: self)!])
            }
        } catch let error {
            print("Invalid regex: \(error.localizedDescription)")
            return []
        }
    }
}
