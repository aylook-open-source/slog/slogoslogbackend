import Foundation
import XCTest
import SLog

@testable import SLogOSBackend

final class OSLogTemplatedStyleMessageConverterTests: XCTestCase {

    let sut = OSLogTemplatedStyleMessageConverter.shared

    func testNoPlaceholdersString() throws {
        let input = "No templated placeholders "
        let result = sut.convertToString(templated: input, arguments: ["1", 2])
        XCTAssertEqual(input, result)
    }

    func testNoProtectionTemplatedString() throws {
        let input = "My name is %@, and im %d years old"
        let args: [TypeWrapperRepresentable] = ["NONAME", 20]

        let result = sut.convertToString(templated: input, arguments: args)
        let properResult = "My name is \(args[0].typeWrappedValue.cVarArg), and im \(args[1].typeWrappedValue.cVarArg) years old"
        XCTAssertEqual(result, properResult)
    }

    func testPublicProtectionTemplatedString() throws {
        let input = "My name is %{public}@, and im %d years old"
        let args: [TypeWrapperRepresentable] = ["NONAME", 20]

        let result = sut.convertToString(templated: input, arguments: args)
        let properResult = "My name is \(args[0].typeWrappedValue.cVarArg), and im \(args[1].typeWrappedValue.cVarArg) years old"
        XCTAssertEqual(result, properResult)
    }

    func testPrivateProtectionTemplatedString() throws {
        let input = "My name is %{private}@, and im %{private}d years old. %d"
        let args: [TypeWrapperRepresentable] = ["NONAME", 20, 0]

        let privateReplacer = OSLogTemplatedStyleMessageConverter.privatePlaceholderReplacement
        let result = sut.convertToString(templated: input, arguments: args)
        let properResult = "My name is \(privateReplacer), and im \(privateReplacer) years old. \(args[2].typeWrappedValue.cVarArg)"
        XCTAssertEqual(result, properResult)
    }
}
